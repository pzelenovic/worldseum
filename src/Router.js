import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import Map from './components/Map';
import LocalityView from './components/LocalityView';

const RouterComponent = () => {
  return (
    <Router sceneStyle={{paddingTop: 0}}>
      <Scene key="root">
        <Scene key="map" component={ Map } title="Worldseum Map" initial />
        <Scene key="localityView" component={ LocalityView } title="Locality" />
      </Scene>
    </Router>
  )
};

export default RouterComponent;
