import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import ViewSection from './ViewSection';

class LocalityView extends Component {

  constructor(props) {
    super(props);

    this.BASE_URL = 'http://35.188.76.185/wmuseum/api/v1/';

    this.state = {
      contentsList: []
    }
  }

  fetchContentsList() {
    let URL = this.BASE_URL + "localities/" + this.props.currentMarker.id + "/contents";

    fetch(URL)
      .then(response => {return response.json()})
      .then(responseJSON => {
        this.setState({
          contentsList: responseJSON
        })
      });
  }

  componentDidMount() {
    this.fetchContentsList();
  }

  render() {
    return (
      <ScrollView style={styles.localityViewStyle}>
        <ViewSection style={styles.localityTitleStyle} textContent={this.props.currentMarker.title}></ViewSection>
        <ViewSection style={styles.localityDescriptionStyle} textContent={this.props.currentMarker.description}></ViewSection>
        <ScrollView style={styles.contentScrollerStyle}>
          {this.state.contentsList.map(contentItem => (
            <ViewSection key={contentItem.id} style={styles.contentItemStyle} textContent={contentItem.title} />
          ))}
        </ScrollView>
      </ScrollView>
    );
  }
}

const styles = {
  localityViewStyle: {
    flex: 1
  },
  localityTitleStyle: {
    flex: 1,
    fontSize: 20,
    padding: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    backgroundColor: 'white'
  },
  localityDescriptionStyle: {
    flex: 1,
    fontSize: 11,
    padding: 10,
    textAlign: 'justify',
    backgroundColor: 'white',
  },
  contentScrollerStyle: {
    flex: 1,
    padding: 5
  },
  contentItemStyle: {
    flex: 1,
    padding: 10,
    backgroundColor: 'beige',
    color: 'blue'
  }
}

export default LocalityView;
