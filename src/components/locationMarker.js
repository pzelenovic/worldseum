import React, { Component } from 'react';
import { Text } from 'react-native';

class LocationMarker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marker: this.props.marker
    }
  }
  
  render() {
    return (
      <Text style={styles.markerTitle} value={marker.title} />
    );
  }
}

const styles = {
  markerTitle: {
    flex: 1,
    fontSize: 20
  }
}

export default LocationMarker;