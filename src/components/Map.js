import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import MapView from 'react-native-maps';
import { Actions } from 'react-native-router-flux';

// markers is an array which stores data on localities' coordinates, names and descriptions
import Markers from '../data/Markers.json';

// location marker is a component that displays locality handle
import LocationMarker from './locationMarker';

class Map extends Component {

  constructor(props) {
    super(props);

    this.BASE_URL = 'http://35.188.76.185/wmuseum/api/v1/';

    this.screenWidth = Dimensions.get('window').width;
    this.screenHeight = Dimensions.get('window').height;
    this.ASPECT_RATIO = this.screenWidth / this.screenHeight;

    this.LATITUDE = 44.817962;
    this.LONGITUDE = 20.456510;
    this.LATITUDE_DELTA = 0.007;
    this.LONGITUDE_DELTA = this.ASPECT_RATIO * this.LATITUDE_DELTA;

    this.state = {
      markers: Markers,
      region: {
        latitude: this.LATITUDE,
        longitude: this.LONGITUDE,
        latitudeDelta: this.LATITUDE_DELTA,
        longitudeDelta: this.LONGITUDE_DELTA
      }
    }
  }

  loadLocation({marker}) {
    Actions.localityView({currentMarker: marker});
  }

  fetchMarkers() {
    let URL = this.BASE_URL + "localities?latitude=44.816456&longitude=20.460121&delta=" + this.LATITUDE_DELTA;

    fetch(URL)
    .then(response => {return response.json()})
    .then(processedResponse => {
      this.setState({markers: processedResponse});
    });
  }

  componentDidMount() {

    this.fetchMarkers();

    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: this.LATITUDE_DELTA,
            longitudeDelta: this.LONGITUDE_DELTA
          }
        });
      },
      error => {
        console.log(error.message);
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
      }
    );

    this.watchID = navigator.geolocation.watchPosition(
      position => {
        this.setState({
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: this.LATITUDE_DELTA,
            longitudeDelta: this.LONGITUDE_DELTA
          }
        })
      }
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  render() {
    return (
      <MapView

        style={styles.mapView}

        region={this.state.region}
        onRegionChange={region => this.setState({region})}
        onRegionChangeComplete={region => this.setState({region})}
        showsUserLocation={ true }
        showsPointsOfInterest={ false }
        zoomEnabled={ true }

      >

        {this.state.markers.map(marker => (
          <MapView.Marker
            key={marker.id}
            coordinate={marker.coordinates}
            title={marker.title}
            description={marker.description}
            onCalloutPress={this.loadLocation.bind(this, {marker})}
            image={markerImage}
            style={{width: 4, height: 4}}
          />
        ))}

      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  mapView: {
    flex: 1
  }
});

const markerImage = require('../assets/images/marker.png');
// const markerImage = require('http://downloadicons.net/sites/default/files/residential-buildings-icons-68726.png');

export default Map;
