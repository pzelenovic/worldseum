import React, { Component } from 'react';
import { View, Text } from 'react-native';

class ViewSection extends Component {

  constructor(props) {
    super(props);

  }

  render() {
    return (
      <View>
        <Text style={this.props.style}>{this.props.textContent}</Text>
      </View>
    );
  }
}

export default ViewSection;
